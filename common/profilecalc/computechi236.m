%
%  chi2grid=computechi21026(erates,ages,inhers,comp,depths,measuredconc,...
%                       sigma,scaling_model)
%
%
%  Computes a 3-d array of Chi^2 values corresponding to the ages,
%  erosion rates, inheritances, given for the profile defined by
%  comp, depths, measuredconc, sigma, and scaling_model.
%
%
function [chi2grid,chi2gridalt,chi2gridalt2]=computechi236(erates,ages,inhers,comp,depths,measuredconc,...
    sigma,scaling_model)
%
% Global Variables.
%
global PP;
global STOREDSP;
global STOREDSF;
global STOREDCP;
%
% First, figure out the dimensions of the problem.
%
nerosion=length(erates);
nages=length(ages);
ninhers=length(inhers);
nsamples=size(comp,1);

measuredconc36=measuredconc(:,1);
sigma36=sigma(:,1);
%
% Initialize chi2grid.
%
chi2grid=zeros(nerosion,nages,ninhers);
chi2gridalt=zeros(nerosion,nages,ninhers);
chi2gridalt2=zeros(nerosion,nages,ninhers);
%
% Loop over the samples.
%
fprintf(1,'Progress: 00.0%%');
for sample=1:nsamples
%
% Compute the parameters for this sample.
%
  pp=PP;
  sp=STOREDSP{sample};
  sf=STOREDSF{sample};
  cp=STOREDCP{sample};
%
% Go ahead and produce contemporary scaling factors.
%
  sf.currentsf=getcurrentsf(sf,0,scaling_model,'cl');
%
% Set the depth to the depth of this sample.
%
  thisdepth=depths(sample);
%
% Compute the contemporary surface production rate and use it to get the
% saturation concentration=production rate/decay rate.
%
  ProdtotalCl=prodz36(0,pp,sf,cp);
%
% Now, loop over all of the erosion rates, ages, and inheritances.
%
  for i=1:nerosion
    %
    % Adjust the erosion rate.
    %
    sp.epsilon=erates(i);
    for j=1:nages
        %
        % Check for the impossible case that with negative erosion, there
        % couldn't be a point at this current depth (thisdepth) that was
        % original deposited back then.
        %
        if (min(depths) < (-erates(i)*ages(j)))
            N36predicted=-Inf;
        else
            [N36predicted]=predN36depth(pp,sp,sf,cp,ages(j),depths(sample),scaling_model,0.1);
        end
        for k=1:ninhers
            %
            % The predicted 10-Be consists of cosmogenic 10-Be plus 
            % inherited 10-Be.  We take the difference between this
            % and the measured concentration of 10-Be in the sample.
            %
            rCl=((N36predicted+inhers(k)*ProdtotalCl-measuredconc36(sample))/...
                sigma36(sample));
            %Brian's original formulation
            chi2grid(i,j,k)=chi2grid(i,j,k)+rCl^2;
            rClalt=((N36predicted+inhers(k)*ProdtotalCl-measuredconc36(sample))^2)/...
                (N36predicted+inhers(k)*ProdtotalCl);
            rClalt2=rClalt/(sigma36(sample)/measuredconc36(sample)*100);
            %this one is just regular chi2
            chi2gridalt(i,j,k)=rClalt;%chi2gridalt(i,j,k)+rClalt;
            %this one is weighted by %uncert for each sample
            chi2gridalt2(i,j,k)=chi2gridalt2(i,j,k)+rClalt2;
        end
    end

    fprintf(1,'\b\b\b\b\b%04.1f%%',100*((sample-1)/nsamples+i/(nerosion*nsamples)));
  end
end

fprintf(1,'\b\b\b\b\b100%% \n');