function [indexofmax,themax]=wheremax(A)
%
%   [indexofmax,themax]=wheremax(A);
%
%   Finds the index of maximum of a 3-d matrix  A, and returns that maximum
%   value.
%
%   Outputs:
%   
%       indexofmax          vector of the indices of the maximum
%       themax              The maximum element in A

[m,n,p]=size(A);
imax=0;
jmax=0;
kmax=0;
currentmax=-inf;

for i=1:m
    for j=1:n
        for k=1:p
            temp = A(i,j,k);
            if (temp > currentmax)
                currentmax=temp;
                imax=i;
                jmax=j;
                kmax=k;
            end
        end
    end
end


indexofmax=[imax jmax kmax];
themax=A(imax,jmax,kmax);