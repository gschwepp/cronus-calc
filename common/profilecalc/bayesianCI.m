function [BCI, P_x] = bayesianCI(x,p_x,alpha)

n = length(x);
P_x = zeros(size(x));
LB = 0;
UB = 0;
for i = 2:n
    P_x(i) = trapz(x(1:i),p_x(1:i));
    if (LB == 0 && P_x(i) > alpha/2)
        LB = (x(i)+x(i-1))/2;
    end
    if (UB == 0 && P_x(i) > 1 - alpha/2)
        UB = (x(i)+x(i-1))/2;
    end
end

BCI = [LB UB];



        