%
% First, load in the predicted concentrations.
%
load agecalib10second.mat
%
% Plot the ratios
%
figure(1);
clf;
plot(ageindex10,ratios,'ko');
hold on
ylabel('Computed Age/Independent Age');
xlabel('site index');
plot([0 max(ageindex10)+1],[1.0 1.0],'k--');
axis([0 max(ageindex10)+1 0 2.0]);
print -depsc agecalibset10second.eps
print -dpdf agecalibset10second.pdf
print -dpng agecalibset10second.png
