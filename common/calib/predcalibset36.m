%
% This script computes ages for the Calibration samples using
% standard production rate. 
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.
%
load calibset36.mat
%
% Get basic info about the sample ages.
%
INDAGES=indages36(:,1)/1000;
SIGMAAGES=indages36(:,2)/1000;
[nsamples,ignore]=size(nominal36);
%
% Now, compute predicted N36 for each sample, and compute ratios.
%
measuredN36=nominal36(:,1);
pred36=zeros(size(measuredN36));
ratios=zeros(size(measuredN36));
uncerts=ratios;

for k=1:nsamples;
  [pp,sp,sf,cp]=getpars36(nominal36(k,:));
  pred36(k)=predN36(pp,sp,sf,cp,INDAGES(ageindex36(k)));
  ratios(k)=measuredN36(k)/pred36(k);
  temp=cl36uncert(nominal36(k,1));
  if (temp > uncerts36(k,1))
    uncerts(k)=temp;
  else
    uncerts(k)=uncerts36(k,1);
  end
  uncerts(k)=uncerts(k)/pred36(k);
  fprintf(1,'Sample %d, N36pred=%f, N36meas=%f, ratio=%f +- %f \n',full(...
      [k; pred36(k); measuredN36(k); ratios(k); uncerts(k)]));
end
%
% Compute the errors.
%
percenterrors=100*(ratios-ones(size(ratios)));
RMSE=sqrt(mean(percenterrors.^2))
%
% Save the results.
%
save predcalibset36.mat
quit
