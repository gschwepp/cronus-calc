This directory contains MATLAB code and data sets for the calibration
of spallation production rates for 10-Be, 26-Al, 36-Cl, 14-C, and
3-He.  The output from these computations is also included, so you
should not need to rerun these computations unless the data or
production model are changed.

Prerequisites: In order to run this code, you will need access to
MATLAB.  The results in this directory were produced using MATLAB 7.10
(R2010A) Later versions of MATLAB should work, but earlier versions of
MATLAB may well fail.  The code in this directory makes extensive use
of routines from the ../production and ../surfacecalc directorys-
please make sure that these are correctly configured for the scaling
scheme that you're interested in.  You must all make sure that these
directories are in your MATLAB search path, by (e.g.) issuing the
commands

>> addpath ../production  
>> addpath ../surfacecalc

Shell scripts: Some programs in this directory take quite a long time
(e.g. days, not hours) to run.  For this reason it is generally best to run
them unattended on a server rather than a desktop or laptop computer.
I have written shell scripts that can automate this process on a Linux
computer.  If you are doing the computations on a Windows machine then
you'll simply want to run the MATLAB scripts from the command line.

1. Calibration of parameters for 10-Be and 26-Al.  

The model of production of 10-Be and 26-Al depends on both spallation
production rates and muon production parameters.  Since the two
calibrations are not completely indpendent, it is necessary to iterate
between calibrating the spallation production rates and the muon
production parameters.  After a few iterations, the production rates
will converge to 3-4 digits.  Note that calibration of muon production
parameters for 10-Be and 26-Al is done simultaneously.  Thus the
spallation production parameters for 10-Be and 26-Al must be
calibrated in tandem.
 
To run the calibration of spallation production rates for 10-Be,
either use the shell script "runcalibrate10", or run the MATLAB script
"calibrate10.m" from the MATLAB command line.  Transfer the resulting
production rate PsBe to ../production/physpars.m.

The script predcalibset10.m predicts the concentrations of 10-Be in
the calibration samples and compares them to the measured
concentrations.  The script plotcalib10.m then produces a plot of the
ratios of measued 10-Be concentration to predicted 10-Be concentration
for the calibration samples.
 
As a check on the stability of the 10-Be calibration we perform "leave one
out" cross validation.  The MATLAB scripts calibrate10minusPPT.m, 
calibrate10minusNZ.m, calibrate10minusSCOT.m  and calibrate10minusPERU.m 
perform these calibrations.  This can also be done using the shell script
runcalibrate10crossval.

As a further check on the calibration of the 10-Be production rate, we
compute the ages for a collection of samples in a secondary data set.
The script agecalibset10second.m computes these ages.  A plot showing
the ratios of the computed ages to the independent site ages is
produced by the script plotcalib10second

Each of the above MATLAB scripts produces corresponding .out files and
in the case of the plotting scripts, the plots are output in .eps,
.pdf, and .png format.

The procedure for calibration of 26-Al spallation production rates is
essentially identical to the procedure for calibration of the 10-Be
production rates, except that the script names have "26" in place of
"10" and the list of cross validation sites is different.  
 
2. Calibration of spallation production rates for 3-He.  

We do not model muonic production for 3-He, so there is no separate
calibration of muonic production rate paramters.  The procedure for
calibration of the spallation production rate is identical to the
procedure for calibration of the spallation production for 10-Be,
except that "10" is replaced with "3" in all of the file names and the
list of cross validation site differs.

3. Calibration of spallation production rates for 14-C.  
 
We do not model muonic production for 14-C, so there is no separate 
calibration of muonic production rate parameters.  There is no secondary
data set for 14-C, so we do not compute ages for a secondary data set.  
Otherwise, the procedure for calibrating the spallation production rate
is identical to the procedure for 10-Be, except that "10" is replaced by
"14" in the file names and the list of cross validation sites differs.  

4. Calibration of spallation production rates for 36-Cl.  
 
36-Cl can be produced by spallation of Ca, K, Fe, and Ti.  Here, we
only calibrate the Ca and K production rates.  Since production from
both Ca and K can occur within a single sample, we calibrate these
production rates simultaneously.  

As with 10-Be and 26-Al, we separately calibrate muonic production
rates (see the ../muoncalib directory) and iterate until the
spallation and muon production rate parameters have converged to 3-4
digits.

Currently, a fixed value of the Pf(0) parameter for production of 36-Cl
by neutron capture is used.  Calibration of this parameter could also
be incorporated into the iterative process.  However, the current calibration
data set was selected to exclude samples with significant production by
neutron capture, so a different calibration data set would be required.  
 
The procedure for calibrating the spallation production rates for 36-Cl from
Ca and K is essentially identical to the procedure for calibration of 10-Be
spallation production rates, except that "36" replaces "10" in the script
names.  There are two secondary data sets for 36-Cl rather than one.  The 
first "quantitive" data set contains samples from sites with independent
age constraints, while the second "qualitative" data set contains samples
from sites with weak or one sided age constraints.  

5. Producing .csv files.  Fred Phillips asked for .csv files containing the
numbers that appear in the plots produced by the various plotting scripts in
this directory.  The script makecsv.m produces these .csv files.  
 
