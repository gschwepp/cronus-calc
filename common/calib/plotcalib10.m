%
% First, load in the predicted concentrations.
%
load predcalibset10.mat
%
% Plot the ratios
%
figure(1);
clf;
errorbar(ageindex10,ratios,uncerts,'ko');
hold on
ylabel('N10meas/N10pred');
xlabel('site index');
plot([0 max(ageindex10)+1],[1.0 1.0],'k--');
axis([0 max(ageindex10)+1 0 2.0]);
print -depsc predcalibset10.eps
print -dpdf predcalibset10.pdf
print -dpng predcalibset10.png
