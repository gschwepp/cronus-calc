%
% This script plots the computed ages for calibset10.
%
load agecalibset10second.mat
%
% First, setup ranges that define which samples are from which
% sites in the 10-Be data set.
%
%  1. BST
%  2. NEBH
%  3. W86
%  4. WY
%  5. YDC
%  6. OL
%  7. JSS      (John Stone Scotland samples)
%
rangebst=1:10;
rangenebh=11:24;
rangew86=25:34;
rangewy=35:43;
rangeydc=44:51;
rangeol=52:58;
rangejss=59:62;
%
% Set up a mapping from sample number to site number.
%
sites=zeros(62,1);
sites(rangebst)=1;
sites(rangenebh)=2;
sites(rangew86)=3;
sites(rangewy)=4;
sites(rangeydc)=5;
sites(rangeol)=6;
sites(rangejss)=7;
%
% Now, get independent ages for the sites.
%
siteages=[INDAGES(1); INDAGES(11); INDAGES(25); INDAGES(35); ...
	  INDAGES(44); INDAGES(52); INDAGES(59)];
siteuncerts=[SIGMAAGES(1); SIGMAAGES(11); SIGMAAGES(25); SIGMAAGES(35); ...
	     SIGMAAGES(44); SIGMAAGES(52); SIGMAAGES(59)];
%
% Plot the results.
%
figure(2);
clf;
errorbar(sites-0.03*mod((1:62)',10),computedages,computeduncerts,'ko');
hold on
errorbar(1.1:7.1,siteages,siteuncerts,'ro');
axis([0 8 0 25]);
xlabel('Site (1=BST, 2=NEBH, 3=W86, 4=WY, 5=YDC, 6=OL, 7=JSS)');
ylabel('Age (kyr before 2010)');
print -dpng plotcalibset10secondages.png
print -depsc plotcalibset10secondages.eps
%
% Compute % errors for all samples and the RMSE.
%
percenterrors=zeros(62,1);
for k=1:62
  percenterrors(k)=100*(computedages(k)/INDAGES(k)-1);
end
rmse=sqrt(mean(percenterrors.^2));
fprintf(1,'RMSE=%f%% \n',rmse);
%
% Output some information about the sites.
%
fprintf(1,['1, BST, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(1); siteuncerts(1); mean(computedages(rangebst)); ...
	 std(computedages(rangebst)); ... 
	 100*(mean(computedages(rangebst))/siteages(1)-1)]);
fprintf(1,['2, NEBH, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(2); siteuncerts(2); mean(computedages(rangenebh)); ...
	 std(computedages(rangenebh)); ... 
	 100*(mean(computedages(rangenebh))/siteages(2)-1)]);
fprintf(1,['3, W86, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(3); siteuncerts(3); mean(computedages(rangew86)); ...
	 std(computedages(rangew86)); ... 
	 100*(mean(computedages(rangew86))/siteages(3)-1)]);
fprintf(1,['4, WY, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(4); siteuncerts(4); mean(computedages(rangewy)); ...
	 std(computedages(rangewy)); ... 
	 100*(mean(computedages(rangewy))/siteages(4)-1)]);
fprintf(1,['5, YDC, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(5); siteuncerts(5); mean(computedages(rangeydc)); ...
	 std(computedages(rangeydc)); ... 
	 100*(mean(computedages(rangeydc))/siteages(5)-1)]);
fprintf(1,['6, OL, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(6); siteuncerts(6); mean(computedages(rangeol)); ...
	 std(computedages(rangeol)); ... 
	 100*(mean(computedages(rangeol))/siteages(6)-1)]);
fprintf(1,['7, JSS, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(7); siteuncerts(7); mean(computedages(rangejss)); ...
	 std(computedages(rangejss)); ... 
	 100*(mean(computedages(rangejss))/siteages(7)-1)]);


                               