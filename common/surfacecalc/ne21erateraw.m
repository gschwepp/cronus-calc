%
%  age=ne21erateraw(pp,sp,sf,cp,maxerate,minrate)
%
%  Given the data for a saturated sample, computes the
%  corresponding erosion rate.
%
% This inner routine does not handle the uncertainty calculations, 
% which are done by ne21erate.m.  Instead, this inner routine simply 
% does the basic computation of the erosion rate.
%
% Inputs:
%    pp,sp,sf,cp       as from getpars21.
%    maxrate           Maximum erosion rate (g/(cm^2*kyr))
%    minrate           Minimum erosion rate (g/(cm^2*kyr))
%                      (optional, depfaults to 0)
%
% Returns 
%
%   erate              g/(cm^2*kyr)
%
%
function [erate,eratemm]=ne21erateraw(pp,sp,sf,cp,scaling_model)
%
% Calculate the average production rate over the entire lifetime of the
% sample at that particular location. We assume 10Ma for Ne. 

%Start with ST (constant scaling)
  %get current scalefactor; we use Be as nuclide because Ne is tied with
  %a ratio to Be
  sf.currentsf=getcurrentsf(sf,0,scaling_model,'be');
    
%calculate the depths to use in the calculation of depth-averaged
%production rate
ndepths=10;
%
thickness=sp.ls;
depths=zeros(ndepths,1);
deltadepth=thickness/ndepths;
for i=1:ndepths
  depths(i)=sp.depthtotop+deltadepth/2+deltadepth*(i-1);
end

[ProdtotalNe,ProdsNe]=prodz21(depths,pp,sf,cp);

%Take avg of all production rates from all the depths within the sample
Pavg=mean(ProdtotalNe);


erateyr=Pavg*cp.Lambdafe/sp.concentration21;
erate=erateyr*1000; %convert from g/cm2/yr to g/cm2/kyr
eratemm=erate/sp.rb*10; %convert to mm/kyr

% % Set a default minimum erosion rate of 0 if none is specified.
% %
% if (nargin < 6)
%   minrate=0.0;
% end
% %
% % Figure out the maximum possible depth at which we'll ever need a
% % production rate.  
% %
% maxage=10000;                             % Technically, Ne can't be 
% % saturated, but for practical purposes, we need a number here. 
% %
% sp.epsilon=minrate;
% % Temporary comment - maxcon check
% %maxcon=predN21(pp,sp,sf,cp,maxage,scaling_model);
% %
% % Make sure that the concentration is reasonable.  
% %
% %if (sp.concentration21 > maxcon)
% %  warning('This sample is oversaturated based on min erosion rate');
% %  erate=NaN;
% %  return;
% %end
% %
% % The main loop does bisection search to find the corresponding
% % erosion rate.
% %
% lowerrate=minrate;
% %find a max erosion rate by starting at 10, checking concentration, and
% %increasing by an order of magnitude until the concentration is within the
% %bounds of search. 
% upperrate=0.1;
% sp.epsilon=upperrate;
% minconc=predN21(pp,sp,sf,cp,maxage,scaling_model);
% i=0;
% 
% while (minconc-sp.concentration21 > 0)
%     upperrate=upperrate*10;
%     i=i+1;
%     sp.epsilon=upperrate;
%     %recalculate the maxdepth so that cp can be found to an appropriate depth
%     maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
%     %
%     % Computed parameters.
%     %
%     cp=comppars21(pp,sp,sf,maxdepth);
%     minconc=predN21(pp,sp,sf,cp,maxage,scaling_model);
%     if i==4
%         warning('Maximum erosion rate (1000g/cm^2) reached - check sample inputs')
%         erate=NaN;
%        
%     end
% end
% 
% while (upperrate-lowerrate > 1.0e-6) % default 1E-6
%   midrate=(upperrate+lowerrate)/2;
%   sp.epsilon=midrate;
%   midcon=predN21(pp,sp,sf,cp,maxage,scaling_model);
%   if (midcon > sp.concentration21)
%     lowerrate=midrate;
%   else
%     upperrate=midrate;
%   end
% end
% erate=(upperrate+lowerrate)/2;
% 
% 
