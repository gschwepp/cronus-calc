
function [ total, computedages, computeduncerts, scalefactor, prodratene, computeduncertsint ] = ...
  usingne21age( scaling_model, inputfilename, outputfilename )
  
load( inputfilename, '-mat' );

%
% Get basic info about the sample ages.
%
%INDAGES=indages21(:,1)/1000;
%SIGMAAGES=indages21(:,2)/1000;
nsamples=size(nominal21,1);
%
% Update the uncertainties on the concentration.
%
% for k=1:nsamples
%   temp=ne21uncert(nominal21(k,9));
%   if (temp > uncerts21(k,9))
%     uncerts21(k,9)=temp;
%   end
% end
%
% Now, compute ages for each sample, with uncertainties.
%
computedages=zeros(nsamples,1);
for k=1:nsamples;
  tic;
  output=ne21age(nominal21(k,:),uncerts21(k,:),scaling_model);
  
  computedages(k)=output(1);
  computeduncerts(k)=output(2);
  scalefactor(k)=output(3);
  prodratene(k)=output(4);
  computeduncertsint(k)=output(8);
  
  fprintf(1,'Ne Sample %d, computed age=%f +- %f in %f minutes\n',full(...
      [k; output(1); output(2); toc/60]));
end
%
% Compute the errors.
%
%percenterrors=100*(computedages-INDAGES)./INDAGES;
%RMSE=sqrt(mean(percenterrors.^2))
%
%To output everything in an "easy to paste into excel" format:
  total(:,1)=computedages';
  total(:,2)=computeduncerts';
  total(:,3)=scalefactor';
  total(:,4)=prodratene';
  total(:,5)=computeduncertsint';

if nargin > 2
  % Save the results.
  %
  save( outputfilename );
end
