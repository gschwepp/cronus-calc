% [erate,uncerts,eratemm,uncertsmm,total]=ne21erate(sampledata,sampleuncertainties,scaling_model)
%
%  Given the data for a sample and associated one standard
%  deviation uncertainties, computes the erosion rate of the sample and uncertainty.
%
% The sampledata vector contains the following information:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (g/(cm^2*kyr))
%9. Sample 21-Ne concentration (atoms of 21-Ne/g of target)
%10. Inheritance for 21-Ne (atoms 21-Ne/g of target)
%11. Effective attenuation length -Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%
% A second input vector, sampleuncertainties, contains 1-sigma
% uncertainties for all 13 inputs.  In general, we assume that
% these 13 inputs are uncorrelated. 
%
% scaling_model is one of 'DE','DU','LI','LM','SA','SF','ST' and 
% informs which scaling model is being used
%
% Returns:
% erate g/(cm^2*kyr) and uncertainty; erate in units of mm/kyr and uncertainty.

function [erate,uncerts,eratemm,uncertsmm,total]=ne21erate(sampledata,sampleuncertainties,scaling_model)
tic;
%
% Make sampledata and uncertainties column vectors if they aren't already.
%
if (size(sampledata,1)==1)
  sampledata=sampledata';
end
if (size(sampleuncertainties,1)==1)
  sampleuncertainties=sampleuncertainties';
end

%
% First, check that the input data is reasonable.
%
if (length(sampledata) ~= 13)
  error('sampledata has wrong size!');
end
if (length(sampleuncertainties) ~= 13)
  error('sampleuncertainties has wrong size!');
end
%if (~exist('scaling_model','var')), scaling_model = 'all'; end
%
% Setup the physical parameters.
%
pp=physpars();
%
% Extract the sample parameters from the sampledatavector.
%
sp=samppars21(sampledata);
%
% Get the scale factors.
%
sf=scalefacs21(sp,scaling_model);
%
% We need an absolute maximum age for several purposes, including
% detecting saturated samples and setting the maximum depth for comppars.
%
maxage=10000;                             % Technically, Ne can't be 
% saturated, but for practical purposes, we need a number here. 
%
% Figure out the maximum possible depth at which we'll ever need a
% production rate.  This is depthtotop + maxage * erosion (g/cm2/kyr) +
% thickness * density + a safety factor. 
maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+2000;
%
% Computed parameters.
%
cp=comppars21(pp,sp,sf,maxdepth);

% Compute the nominal results. Assumes min erosion rate is 0.
%
erate=ne21erateraw(pp,sp,sf,cp,scaling_model);
eratemm=erate/sp.rb*10;
%%%%%%%% Uncertainty calcs
% Start off with a sum of 0.
%
uncertainty=0.0;
%
% Work through all 13 sample parameters.  For each nonzero
% uncertainty, add in the uncertainty term.
%
derivs=zeros(13,1);
for i=1:13
  if ((sampleuncertainties(i) ~= 0.0))
    if (sampledata(i) ~= 0.0)
      thisdelta=0.01*abs(sampledata(i));
    else
      thisdelta=0.01;
    end
    deltasampledata=sampledata;
    deltasampledata(i)=deltasampledata(i)+thisdelta;
    deltasp=samppars21(deltasampledata);
    deltasf=scalefacs21(deltasp,scaling_model);
    deltacp=comppars21(pp,deltasp,deltasf,maxdepth);
    deltaoutput=ne21erateraw(pp,deltasp,deltasf,deltacp,scaling_model);
    deltaerate=deltaoutput(1);
    deratei=(deltaerate-erate)/(thisdelta);
    derivs(i)=deratei;
    if (~isnan(sampleuncertainties(i)))
      uncertainty=uncertainty+(deratei^2*sampleuncertainties(i)^2);
    end
  end
end
%
% Add in terms for the uncertainty in production rates.  
%
%
% Uncertainty in PsNe.
%
deltapp=pp;
deltapp.PsNe=pp.PsNe+0.01*abs(pp.PsNe);
deltaoutput=ne21erateraw(deltapp,sp,sf,cp,scaling_model);
deltaerate=deltaoutput(1);
deratepsNe=(deltaerate-erate)/(0.01*abs(pp.PsNe));
uncertainty=uncertainty+(deratepsNe^2*pp.sigmaPsNe^2);
%
% Finally, take the square root of uncertainty to get a standard deviation.
%
uncerts=sqrt(uncertainty);
%convert to other units
uncertsmm=uncerts/erate*eratemm;
toc;
total(:,1)=erate;
total(:,2)=uncerts;
total(:,3)=eratemm;
total(:,4)=uncertsmm;