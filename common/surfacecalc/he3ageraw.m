%
%  age=he3ageraw(sampledata,pp,sf,scaling_model)
%
%  Given the data for a sample computes the age of the sample.
%
%
% This inner routine does not handle the uncertainty calculations, 
% which are done by he3age.m.  Instead, this inner routine simply 
% does the basic computation of the age.
%
% The sampledata vector contains the following information:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (g/(cm^2*kyr))
%9. Sample 3-He concentration (atoms of 3-He/g of target)
%10. Inheritance for 3-He (atoms 3-He/g of target)
%11. Effective attenuation length -Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%
% Also requires physical parameters (pp) and scaling factors (sf).
%
% Returns an output vector:
%
% 1. age (kyr)
% 2. age uncertainty (always 0 in this routine)
% 3. Contemporary Elevation/latitude scaling factor for neutrons for He (unitless)
% 4. Contemporary depth avg prod rate, neutron spallation (atoms/g/yr)
% 5. Qs (unitless)
% 6. Inherited 3-He (atoms/g of target)
% 7. Measured 3-He (atoms/g of target)
% 8. Analytical (internal) uncertainty (kyr) (always 0 in this routine)
%
function output=he3ageraw(sampledata,pp,sf,scaling_model)
%
% Make sampledata a column vectors if it isn't already.
%
if (size(sampledata,1)==1)
  sampledata=sampledata';
end
%
% First, check that the input data is reasonable.
%
if (length(sampledata) ~= 13)
  error('sampledata has wrong size!');
end
%
% Setup the values of sample parameters.
%
sp=samppars3(sampledata);
%
% Figure out the maximum possible depth at which we'll ever need a
% production rate.  This is depthtotop + maxage * erosion +
% thickness * density + a safety factor.
%
maxage=2500; %Change this to be larger (8000) for samples that are older. 
maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
%
% Computed parameters.  
%
cp=comppars3(pp,sp,sf,maxdepth);
%
% Compute the age. 
%
age=computeage3(pp,sp,sf,cp,scaling_model);
%
% Next, compute production rates for various pathways.
% Here, we call prodz once more at time 0 (present) and then
% compute the depth average from the results.
%
%
% In doing this computation, use the contemporary production rate.
%
sf.currentsf=getcurrentsf(sf,0,scaling_model,'he');
%
%
%
nlayers=100;
thickness=sp.ls*sp.rb;
thick=(thickness/nlayers)*ones(nlayers,1);
totalthickness=sum(thick);
midpt=sp.depthtotop+(thickness/nlayers)*((1:nlayers)'-0.5);
PsHe=prodz3(midpt,pp,sf,cp);
srate=(thick'*PsHe)/totalthickness;
Qs=srate/PsHe(1);
%
% Setup the output vector.
%
output=[age; 0; sf.currentsf.Sel3; srate; Qs; ...
	sp.inheritance3; sp.concentration3; 0];



