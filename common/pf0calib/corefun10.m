function r=corefun10(p)
%
% Global parameters.
%
global samples;
global depths;
global E;
%
% Get the number of samples.
%
[nsamples,foo]=size(samples);
%
% Deal with the case that either parameter is negative.
%
if (p(1)<0)
  r=1.0e30*ones(nsamples,1);
  return
end
if (p(2)<0)
  r=1.0e30*ones(nsamples,1);
  return
end
%
% Pick out the first sample for lat/lon etc.
%
sample=samples(1,:);
%
% Put parameter 1 into the erosion rate.
%
sample(8)=p(1);
%
% Put parameter 2 into the attenuation length.
%
sample(13)=p(2);
%
% Set a maximum depth.
%
maxdepth=20000;
%
% Get the rest of the parameters.
%
pp=physpars();
sp=samppars1026(sample);
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We'll use an age of 10 million years.
%
age=10000;
%
% Loop through the samples and compute predicted concentrations and residuals. 
%
r=zeros(nsamples,1);
[N10,N26]=predN1026depth(pp,sp,sf,cp,age,depths);
for i=1:nsamples
  r(i)=(N10(i)-samples(i,9))/E(i);
end
