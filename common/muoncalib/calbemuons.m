%
% This script uses the Beacon Hill 10-Be profile to calibrate the
% muon production rate.  
%
% Load in the data.
%
load BHC1026StoneB.mat  
%
% Global variables.
%
global E;
global samples;
global depths;
%
% Take only the deeper samples.
%
range=20:32;
depths=nominal10(range,14)+0.5*nominal10(range,5).*nominal10(range,6);
samples=nominal10(range,:);
uncerts=uncerts10(range,:);
%
% Set the erosion rate and atteuation length for 10-Be from
% the calibration of spallation production in near surface samples.
% see calbhcoreboth.out
%
samples(:,8)=0.017274;
samples(:,13)=146.981758;
%
% Get the number of samples.
%
[nsamples,fifteen]=size(samples);
%
% Compute uncertainties on the concentrations.
%
for k=1:nsamples
  E(k)=be10uncert(samples(k,9));
end
%
% An initial guess at the parameters.
%
npars=2;
pinit=[0.25; 4.0];
%
% Use LM to find optimal values of parameters.
%
[pstar,iter]=lm('muonfun10','muonjac10',pinit,1.0e-4,100);
%
% Compute the residual and J at the optimal parameters.
%
rstar=muonfun10(pstar);
Jstar=muonjac10(pstar);
%
% Compute Chi2 and pvalue.
%
chi2=norm(rstar,2)^2;
pvalue=1-chi2cdf(chi2,nsamples-npars);
%
% Compute the covariance matrix for the fitted parameters.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
fprintf(1,'Chi^2=%f, p-value=%f \n',[chi2; pvalue]);
fprintf(1,'sigma0=%f +- %f \n',[pstar(1); ...
		    sigmapstar(1)]);
fprintf(1,'fstar=%f +- %f  \n',[pstar(2); ...
		    sigmapstar(2)]);

for i=1:npars
  for j=1:npars
    corp(i,j)=covp(i,j)/(sqrt(covp(i,i))*sqrt(covp(j,j)));
  end
end
corp
%
% Save the results.
%
save calbemuons.mat
