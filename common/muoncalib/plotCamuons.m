%
% This script plots the results from calClCacore.m
%
%
% Load in the .mat file containing the results.
%
load calClCaprofile.mat
%
% Set the attenuation length for spallation production and the
% erosion rate to the best fitting parameters.
%
nominal36(:,12)=pstar(2);
nominal36(:,3)=pstar(1);
%
% Adjust the depths to correct for the estimated overburden.
%
workingdepths=depths;
for k=(nsurfacesamples+1):nsamples
  workingdepths(k)=depths(k)+pstar(3);
end
%
% Compute predicted 36-Cl using the fitted parameters.
%
maxdepth=150000;
age=10000;
pp=physpars();

N36pred=zeros(size(workingdepths));
N36predc=zeros(size(workingdepths));
for k=1:nsamples
  sp=samppars36(nominal36(k,:));
  sf=scalefacs36(sp);
  cp=comppars36(pp,sp,sf,maxdepth);
  N36predc(k)=predN36depth(pp,sp,sf,cp,age,workingdepths(k),'sa');
  N36pred(k)=N36predc(k)+cp.N36r;
end
%
% Plot the 36-Cl profile.
%
figure(1);
clf;
errorbar(workingdepths,samples(:,1),E,'ko');
axis([0 5500 0 1000000]);
set(gca,'Yscale','log');
hold on
plot(workingdepths,N36pred,'k.');
ylabel('36-Cl Concentration (atoms/gram)-Ca');
xlabel('Depth (grams/cm^2)');
print -dpng plotcoreCa.png
print -dpdf plotcoreCa.pdf
print -deps plotcoreCa.eps