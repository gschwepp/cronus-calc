function J=corejac1026(p)
%
% This version is for simultaneous calibration of the 10-Be and
% 26-Al attenuation lengths, the erosion rate, and the muon
% production parameters for 10-Be and 26-Al.  
%
% The parameters are:
%   p(1)             erosion rate
%   p(2)             attenuation length for 10-Be
%   p(3)             attenuation length for 26-Al
%   p(4)             fstar10 (scaled by 1.0e-3)
%   p(5)             sigma010 (scaled by 1.0e-30)
%   p(6)             fstar26 (scaled by 1.0e-3)
%   p(7)             sigma026 (scaled by 1.0e-30)
%
% Global parameters.
%
global samples;
global depths;
global E;
global nsamples10;
global nsamples26;
global nresids;
global npars;
%
% Make space for the J matrix.
%
J=zeros(nresids,npars);
%
% Compute the derivative with respect to p(1).
%
r0=corefun1026(p);
p1=p;
p1(1)=p(1)*1.001;
r1=corefun1026(p1);
J(:,1)=(r1-r0)/(0.001*p(1));
%
% Compute the derivative with respect to p(2).
%
p1=p;
p1(2)=p(2)*1.001;
r1=corefun1026(p1);
J(:,2)=(r1-r0)/(0.001*p(2));
%
% Compute the derivative with respect to p(3).
%
p1=p;
p1(3)=p(3)*1.001;
r1=corefun1026(p1);
J(:,3)=(r1-r0)/(0.001*p(3));
%
% Compute the derivative with respect to p(4).
%
p1=p;
p1(4)=p(4)*1.001;
r1=corefun1026(p1);
J(:,4)=(r1-r0)/(0.001*p(4));
%
% Compute the derivative with respect to p(5).
%
p1=p;
p1(5)=p(5)*1.001;
r1=corefun1026(p1);
J(:,5)=(r1-r0)/(0.001*p(5));
%
% Compute the derivative with respect to p(6).
%
p1=p;
p1(6)=p(6)*1.001;
r1=corefun1026(p1);
J(:,6)=(r1-r0)/(0.001*p(6));
%
% Compute the derivative with respect to p(7).
%
p1=p;
p1(7)=p(7)*1.001;
r1=corefun1026(p1);
J(:,7)=(r1-r0)/(0.001*p(7));
