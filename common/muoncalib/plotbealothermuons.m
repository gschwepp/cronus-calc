%
% This script plots the secondary measurements from the Beacon
% Heights Core in comparison with model predictions.
%
% Load in the .mat file containing the results of the parameter fitting.
%
load calbhcore.mat
%
% Load in the secondary data.
%
load BHC10other.mat
%
% Get the samples that we need.
%
samples=nominal10;
uncerts=uncerts10;
[nsamples10,fifteen]=size(samples);
E=zeros(2*nsamples10,1);
%
% Separate out the depths.
%
depths=samples(:,14)+0.5*samples(:,5).*samples(:,6);
%
% Compute uncertainties on the concentrations.
%
for k=1:nsamples10
  temp=be10uncert(samples(k,9));
  if (temp > uncerts(k,9))
     E(k)=temp;
  else
     E(k)=uncerts(k,9);
  end
end
%
% Note that some samples have no measured 26-Al, so we'll set those
% residuals to 0 in computing Chi^2.
%
for k=1:nsamples10
  temp=al26uncert(samples(k,10));
  if (samples(k,10) > 0)
    if (temp > uncerts(k,10))
      E(k+nsamples10)=temp;
    else
      E(k+nsamples10)=uncerts(k,10);
    end
  else
    E(k+nsamples10)=1;
  end
end
%
% Compute predicted 10-Be using the fitted parameters.
%
%
% First, setup the correct values of the erosion rate and
% attenuation length.
%
samples(:,8)=pstar(1);
samples(:,13)=pstar(2);
%
% Now, compute the parameters.
%
maxdepth=20000;
pp=physpars();
sp=samppars1026(samples(1,:));
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We need a range of depths from 0 to 6500 g/cm^2.  
%
plotteddepths=(0:125:6500)';
%
% We're using an age of 10 million years (long enough to reach saturation.)
%
age=10000;
%
% Compute the predicted concentrations.
%
[predictedN10,ignore]=predN1026depth(pp,sp,sf,cp,age,plotteddepths);
%
% Plot the 10-Be profile.
%
figure(5);
clf;
errorbar(depths,samples(:,9),E(1:nsamples10),'ko');
axis([0 6500 0 80000000]); 
set(gca,'YScale','log');
hold on
plot(plotteddepths,predictedN10,'k');
ylabel('10-Be Concentration (atoms/g)');
xlabel('Depth (g/cm^2)');
print -dpng plotbeothermuons.png
print -depsc plotbeothermuons.eps
print -dpdf plotbeothermuons.pdf
%
% Compute predicted 26-Al using the fitted parameters.
%
%
% Pick out only the samples that have 26-Al concentrations.
%
range=(samples(:,10) ~= 0);
samples=samples(range,:);
depths=depths(range,:);
rangeAl=[zeros(nsamples10,1); range];
%
% Next, setup the correct values of the erosion rate and
% attenuation length.  Note that attenuation length is different
% for 26-Al!
%
samples(:,8)=pstar(1);
samples(:,13)=pstar(3);
%
% Now, compute the parameters.
%
maxdepth=20000;
pp=physpars();
sp=samppars1026(samples(1,:));
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We need a range of depths from 0 to 6500 g/cm^2.  
%
plotteddepths=(0:125:6500)';
%
% We're using an age of 10 million years (long enough to reach saturation.)
%
age=10000;
%
% Compute the predicted concentrations.
%
[ignore,predictedN26]=predN1026depth(pp,sp,sf,cp,age,plotteddepths);
%
% Plot the 26-Al profile.
%
figure(6);
clf;
errorbar(depths,samples(:,10),E(rangeAl ~= 0),'ko');
axis([0 6500 0 300000000]); 
set(gca,'YScale','log');
hold on
plot(plotteddepths,predictedN26,'k');
ylabel('26-Al Concentration (atoms/g)');
xlabel('Depth (g/cm^2)');
print -dpng plotalothermuons.png
print -depsc plotalothermuons.eps
print -dpdf plotalothermuons.pdf
