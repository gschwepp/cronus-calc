function [pstar, sigmapstar, chi2, pvalue]=calibratemuonK(scaling_model)
%
% This script uses a depth profile to calibrate the
% muon production parameters for production of 36-Cl from K.
%
% Load in the data.
%
load calib36coreK.mat
%
% Global variables.
%
global E;
global samples;
global depths;
%
% Take only the deeper samples.
%
samples=nominal36;
depths=nominal36(:,37)+0.5*nominal36(:,5).*nominal36(:,6);
%
% Get the number of samples.
%
[nsamples,thirtysix]=size(samples);
%
% Compute uncertainties on the concentrations.
%
for k=1:nsamples
   E(k)=uncerts36(k,1);
   if (cl36uncert(nominal36(k,1))> E(k))
     E(k)=cl36uncert(nominal36(k,1));
   end
end
%
% An initial guess at the parameters.
%
npars=4;
pinit=[0.02; 160; 3.5; 1.5];
%
% Use LM to find optimal values of parameters.
%
[pstar,iter]=lm('corefun36K','corejac36K',pinit,1.0e-5,15,scaling_model);
%
% Compute the residual and J at the optimal parameters.
%
rstar=corefun36K(pstar,scaling_model);
Jstar=corejac36K(pstar,scaling_model);
%
% Compute Chi2 and pvalue.
%
chi2=norm(rstar,2)^2;
pvalue=1-chi2cdf(chi2,nsamples-npars);
%
% Compute the covariance matrix for the fitted parameters.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
fprintf(1,'Chi^2=%f, p-value=%f \n',[chi2; pvalue]);
fprintf(1,'erosion rate=%f +- %f \n',[pstar(1); ...
		    sigmapstar(1)]);
fprintf(1,'attenuation length=%f +- %f  \n',[pstar(2); ...
		    sigmapstar(2)]);
fprintf(1,'fstar=%f +- %f \n',[pstar(3); ...
		    sigmapstar(3)]);
fprintf(1,'sigma0=%f +- %f  \n',[pstar(4); ...
		    sigmapstar(4)]);


for i=1:npars
  for j=1:npars
    corp(i,j)=covp(i,j)/(sqrt(covp(i,i))*sqrt(covp(j,j)));
  end
end
corp
%
% Save the results.
%
save calClKprofile.mat
