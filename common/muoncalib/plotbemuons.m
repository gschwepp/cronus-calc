%
% This script plots the results from calbemuons.m
%
%
% Load in the .mat file containing the results.
%
load calbemuons.mat
%
% Compute predicted 10-Be using the fitted parameters.
%
maxdepth=20000;
pp=physpars();
pp.sigma010=pstar(1)*1.0e-30;
pp.fstar10=pstar(2)*1.0e-3;
sp=samppars1026(samples(1,:));
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We need a range of depths from 0 to 6500 g/cm^2.  
%
plotteddepths=(0:250:6500)';
%
% We're using an age of 10 million years (long enough to reach saturation.)
%
age=10000;
%
% Compute the predicted concentrations.
%
[predictedN10,predictedN26]=predN1026depth(pp,sp,sf,cp,age,plotteddepths);
%
% Note that in the following you'll need a copy of herrorbar from
% the MATLAB file-exchange.  
%
%
% Plot the 10-Be profile.
%
figure(3);
clf;
herrorbar(samples(:,9),depths,E(1:nsamples),'ko');
set(gca,'YDir','reverse');
axis([0 200000 0 6500]); 
hold on
plot(predictedN10,plotteddepths,'k');
xlabel('10-Be Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng plotbemuons.png
print -depsc plotbemuons.eps
