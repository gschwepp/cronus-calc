function out=createcalib36xval(sample)

% Uses the standard input for a chlorine-36 sample 
% and creates the individual
% variables needed for calibration in the rest of the code. Produces full
% calibration dataset plus cross validation datasets. Each dataset produces
% a site-specific dataset plus the remaining dataset minus the same
% specific site. It produces this for each site. 

%sample input must be the inputs
%with no spaces between: inputs,uncerts,covariance, independent ages 
%with uncerts, age index. 
% One row per sample with the following columns:

%1. 7.     Latitude (decimal degrees)
%2. 8.     Longitude (decimal degrees)
%3. 9.     Elevation (meters)
%4. 10.    Pressure (hPa)                 
%5. 6.     sample thickness (cm)
%6. 5.     bulk density (g/cm^3)
%7. 11.    Shielding factor for terrain, snow, etc. (unitless)
%8. 3.     erosion-rate epsilon (mm/kyr)
%9. 1.     Sample 36-Cl concentration (atoms of 36-Cl/g of target)
%10. 2.     Inheritance (atoms 36-Cl/g of target)  
%11. 12.    Lambdafe Effective neutron attenuation length (g/cm^2)
%12. 38.    Depth to top of sample (g/cm^2)
%13. 39.    Year Collected (AD)
%14. 4.     fractional volumetric water-content (unitless) 
%15. 13.    wt % SiO2                    Rock
%16. 14.    wt % TiO2                    Rock
%17. 15.    wt % Al2O3                   Rock
%18. 16.    wt % Fe2O3                   Rock
%19. 17.    wt % MnO                     Rock
%20. 18.    wt % MgO                     Rock
%21. 19.    wt % CaO                     Rock
%22. 20.    wt % Na2O                    Rock
%23. 21.    wt % K2O                     Rock
%24. 22.    wt % P2O5                    Rock
%25. 23.    wt % Analytical Water        Rock
%26. 24.    wt % CO2                     Rock
%27. 25.    Cl (ppm)                     Rock
%28. 26.    B (ppm)                      Rock
%29. 27.    Sm (ppm)                     Rock
%30. 28.    Gd (ppm)                     Rock
%31. 29.    U (ppm)                      Rock
%32. 30.    Th (ppm)                     Rock
%33. 31.    Cr (ppm)                     Rock
%34. 32.    Li (ppm)                     Rock
%35. 33.	Target element %K2O          Target
%36. 34.    Target element %CaO          Target
%37. 35.    Target element %TiO2         Target
%38. 36.    Target element %Fe2O3        Target
%39. 37.    Target element Cl (ppm)      Target

%40-78  Uncerts (one for each input above)
%79. 81.    Covariance between cl and at36Cl/g
%80. 79.    Calibration age (years, NOT ka)
%81. 80.    Calibration age uncertainty (years)
%82.    Age Index indicator (should increment for each new age, even within
%         sites (ex. if boulder ages are allowed to vary within the site, 
%         they should each have their own age index))
%83. Site indicator: should match the sites given for the calibration list
%here:
% 1 - Promontory Point & Tabernacle Hill
% 2 - New Zealand
% 3 - Peru
% 4 - Scotland
% 5 - New England
% 6 - Argentina (Ackert 2003) (ARG)
% 7 - Oregon (Cerling & Craig, 1994; Licciardi 1999) (OR)
% 8 - Idaho (Cerling & Craig, 1994) (ID)
% 9 - Canary Islands (Dunai & Wijbrans 2000) (CI)
%10 - Iceland (Licciardi 2006) (ICE)
%11 - Hawaii (Kurz 1990) (HAW)
%12 - Sicily (Blard 2006) (ETNA)
%13 - Antarctica
%14 - Sierra Nevada/White Mountains
%15 - Chile (Transect)


% Outputs of the code:
%1.     Sample 36-Cl concentration (atoms of 36-Cl/g of target)
%2.     Inheritance (atoms 36-Cl/g of target)  
%3.     erosion-rate epsilon ((g/cm^2)/kyr)
%4.     fractional volumetric water-content (unitless) 
%5.     bulk density (g/cm^3)
%6.     sample thickness (cm)
%7.     Latitude (decimal degrees)
%8.     Longitude (decimal degrees)
%9.     Elevation (meters)
%10.    Pressure (hPa)                Either 9 or 10 must be NaN. 
%11.    Shielding factor for terrain, snow, etc. (unitless)
%12.    Lambdafe Effective neutron attenuation length (g/cm^2)
%13.    % CO2                        Rock
%14.    % Na2O                       Rock
%15.    % MgO                        Rock
%16.    % Al2O3                      Rock
%17.    % SiO2                       Rock
%18.    % P2O5                       Rock
%19.    % K2O                        Rock
%20.    % CaO                        Rock
%21.    % TiO2                       Rock
%22.    % MnO                        Rock
%23.    % Fe2O3                      Rock
%24.    Cl (ppm)                     Rock
%25.    B (ppm)                      Rock
%26.    Sm (ppm)                     Rock
%27.    Gd (ppm)                     Rock
%28.    U (ppm)                      Rock
%29.    Th (ppm)                     Rock
%30.    Cr (ppm)                     Rock
%31.    Li (ppm)                     Rock
%32.	Target element %K2O          Target
%33.    Target element %CaO          Target
%34.    Target element %TiO2         Target
%35.    Target element %Fe2O3        Target
%36.    Target element Cl (ppm)      Target
%37.    Depth to top of sample (g/cm^2)
%38.    Year Collected (AD)

fullsitenames36={'TAB';'NZ';'PERU';'SCOT';'NE';'ARG';'OR';'ID';'CI';'ICE';...
    'HAW';'ETNA';'ANT';'WMDV';'NCHL';};
numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
nominal36=zeros(numbersamps,38);
indages36=zeros(numbersamps,2);
uncerts36=zeros(numbersamps,38);
cov36=zeros(numbersamps,1);

%copy in the values directly from the inputs

nominal36(:,1)=sample(:,9);
nominal36(:,2)=sample(:,10);
nominal36(:,3)=sample(:,8);
nominal36(:,4)=sample(:,14);
nominal36(:,5)=sample(:,6);
nominal36(:,6)=sample(:,5);
nominal36(:,7)=sample(:,1);
nominal36(:,8)=sample(:,2);
nominal36(:,9)=sample(:,3);
nominal36(:,10)=sample(:,4);
nominal36(:,11)=sample(:,7);
nominal36(:,12)=sample(:,11);

nominal36(:,13)=sample(:,26);
nominal36(:,14)=sample(:,22);
nominal36(:,15)=sample(:,20);
nominal36(:,16)=sample(:,17);
nominal36(:,17)=sample(:,15);
nominal36(:,18)=sample(:,24);
nominal36(:,19)=sample(:,23);
nominal36(:,20)=sample(:,21);
nominal36(:,21)=sample(:,16);
nominal36(:,22)=sample(:,19);
nominal36(:,23)=sample(:,18);

nominal36(:,24:36)=sample(:,27:39);
nominal36(:,37)=sample(:,12);
nominal36(:,38)=sample(:,13);

uncerts36(:,1)=sample(:,48);
uncerts36(:,2)=sample(:,49);
uncerts36(:,3)=sample(:,47);
uncerts36(:,4)=sample(:,53);
uncerts36(:,5)=sample(:,45);
uncerts36(:,6)=sample(:,44);
uncerts36(:,7)=sample(:,40);
uncerts36(:,8)=sample(:,41);
uncerts36(:,9)=sample(:,42);
uncerts36(:,10)=sample(:,43);
uncerts36(:,11)=sample(:,46);
uncerts36(:,12)=sample(:,50);

uncerts36(:,13)=sample(:,65);
uncerts36(:,14)=sample(:,61);
uncerts36(:,15)=sample(:,59);
uncerts36(:,16)=sample(:,56);
uncerts36(:,17)=sample(:,54);
uncerts36(:,18)=sample(:,63);
uncerts36(:,19)=sample(:,62);
uncerts36(:,20)=sample(:,60);
uncerts36(:,21)=sample(:,55);
uncerts36(:,22)=sample(:,58);
uncerts36(:,23)=sample(:,57);

uncerts36(:,24:36)=sample(:,66:78);
uncerts36(:,37)=sample(:,51);
uncerts36(:,38)=sample(:,52);

cov36(:)=sample(:,79);
indages36(:,1:2)=sample(:,80:81);
ageindexsample36(:,1)=sample(:,82);
siteindexsample36=sample(:,83);

%deal with water content
qavgtemp=nominal36(:,4);

%determine the normalization factor based on water content
for i=1:numbersamps;
    normalization(i)=1-(qavgtemp(i)/sample(i,5));

    % multiply the weight % oxide by the normalization factor to get the actual
    % wt % water
    analyticalwater(i)=sample(i,25)*normalization(i);

    %convert the wt % water to vol water then add them together
    analyticalwatervol(i)=analyticalwater(i)*sample(i,5)/100;
    watervolpercent(i)=qavgtemp(i)+analyticalwatervol(i);

    nominal36(i,4)=watervolpercent(i);

    %combine uncerts on water content
    uncertwater1(i)=sample(i,65)*normalization(i)*sample(i,5)/100;
    uncertwater(i)=sqrt(uncertwater1(i)^2+sample(i,53)^2);
    uncerts36(i,4)=uncertwater(i);
    
    %normalize the values of the major elements
    nominal36(i,15)=nominal36(i,15)*normalization(i);
    nominal36(i,16)=nominal36(i,16)*normalization(i);
    nominal36(i,17)=nominal36(i,17)*normalization(i);
    nominal36(i,18)=nominal36(i,18)*normalization(i);
    nominal36(i,19)=nominal36(i,19)*normalization(i);
    nominal36(i,20)=nominal36(i,20)*normalization(i);
    nominal36(i,21)=nominal36(i,21)*normalization(i);
    nominal36(i,22)=nominal36(i,22)*normalization(i);
    nominal36(i,23)=nominal36(i,23)*normalization(i);
    nominal36(i,24)=nominal36(i,24)*normalization(i);
    nominal36(i,25)=nominal36(i,25)*normalization(i);

    %convert erosion rate and replace in nominal & uncerts
    nominal36(i,3)=nominal36(i,3)*nominal36(i,5)/10;
    uncerts36(i,3)=uncerts36(i,3)*nominal36(i,5)/10;
end

%split up samples based on aging index/site index
%initialize the matrix with the first sample
ageindex36(1)=1;
previousindex=ageindexsample36(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenew36(1,1:2)=indages36(1,1:2);
countindage=1;

for i=2:size(nominal36,1);
    currentindex=ageindexsample36(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindex36(i)=ageindex36(i-1);
    else
        countindage=countindage+1;
        ageindex36(i)=ageindex36(i-1)+1;
        indagenew36(countindage,1:2)=indages36(i,1:2);
        
    end
    previousindex=currentindex;
end

%sorting site index
nameindex36(1)=siteindexsample36(1);
previousnameindex=siteindexsample36(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
countnameindex=1;
sitenames36(1)=fullsitenames36(siteindexsample36(1));
siteindex36(1)=1;
for i=2:size(nominal36,1);
    currentnameindex=siteindexsample36(i);
    if currentnameindex==previousnameindex;
        siteindex36(i)=countnameindex;
    else
        countnameindex=countnameindex+1;
        nameindex36(countnameindex)=siteindexsample36(i);
        siteindex36(i)=countnameindex;
        sitenames36(countnameindex)=fullsitenames36(nameindex36(countnameindex));
    end
    previousnameindex=currentnameindex;
end

indages36old=indages36;
indages36=indagenew36;
save calibset36 nominal36 uncerts36 indages36 ageindex36 cov36 siteindex36 sitenames36

%Below this point - cross-validation datasets are produced

fullnominal36=nominal36;
fulluncerts36=uncerts36;
fullindages36=indages36old;
fullageindex36=ageindex36';
fullcov36=cov36;
fullsiteindex36=siteindexsample36;

for i=1:15
    switch i
        case 1;
            smallname='TABonly36';
            bigname='calib36minusTAB';
            countsiteindex=1;
            
        case 2;
            countsiteindex=2;
            smallname='NZonly36';
            bigname='calib36minusNZ';
            
        case 3;
            countsiteindex=3;
            smallname='PERUonly36';
            bigname='calib36minusPERU';
            
        case 4;
            countsiteindex=4;
            smallname='SCOTonly36';
            bigname='calib36minusSCOT';
        case 5;
            countsiteindex=5;
            smallname='NEonly36';
            bigname='calib36minusNE';
        
        case 6;
            countsiteindex=6;
            smallname='ARGonly36';
            bigname='calib36minusARG';
        case 7;
            countsiteindex=7;
            smallname='ORonly36';
            bigname='calib36minusOR';
        case 8;
            countsiteindex=8;
            smallname='IDonly36';
            bigname='calib36minusID';
        case 9;
            countsiteindex=9;
            smallname='CIonly36';
            bigname='calib36minusCI'; 
        case 10;
            countsiteindex=10;
            smallname='ICEonly36';
            bigname='calib36minusICE';
        case 11;
            countsiteindex=11;
            smallname='HAWonly36';
            bigname='calib36minusHAW';
        case 12;
            countsiteindex=12;
            smallname='ETNAonly36';
            bigname='calib36minusETNA';
        case 13;
            countsiteindex=13;
            smallname='ANTonly36';
            bigname='calib36minusANT';
        case 14;
            countsiteindex=14;
            smallname='SNonly36';
            bigname='calib36minusSN';
        case 15;
            countsiteindex=15;
            smallname='CHILEonly36';
            bigname='calib36minusCHILE';

    end
    countsmall=1;
    countbig=1;
    datafound=false;
    numbersamps=size(fullnominal36,1);
    for k=1:numbersamps;
        if fullsiteindex36(k)==countsiteindex;
            %copy sample(k) to small matrix
            nominalsmall(countsmall,:)=fullnominal36(k,:);
            uncertssmall(countsmall,:)=fulluncerts36(k,:);
            indagessmall(countsmall,:)=fullindages36(k,:);
            ageindexsmalltemp(countsmall,:)=fullageindex36(k,:);
            siteindexsmalltemp(countsmall)=fullsiteindex36(k);
            covsmall(countsmall,:)=fullcov36(k,:);
            countsmall=countsmall+1;
            datafound=true;
            %create the age index and indpt ages matrices
        else
            % copy sample(k) to big matrix
            nominalbig(countbig,:)=fullnominal36(k,:);
            uncertsbig(countbig,:)=fulluncerts36(k,:);
            indagesbig(countbig,:)=fullindages36(k,:);
            ageindexbigtemp(countbig,:)=fullageindex36(k,:);
            siteindexbigtemp(countbig)=fullsiteindex36(k);
            covbig(countbig,:)=fullcov36(k,:);
            countbig=countbig+1;
        end
    end
    % save variables to the correct name (renaming them)
    if datafound==false
            clear nominalsmall
    clear uncertssmall
    clear indagessmall
    clear nominalbig
    clear uncertsbig
    clear indagesbig
    clear ageindexbig
    clear ageindexsmall
    clear ageindexsmalltemp
    clear ageindexbigtemp
    clear indagenewsmall
    clear indagenewbig
    clear covsmall
    clear covbig
    clear siteindexsmalltemp
    clear siteindexbigtemp
        continue
    end 
    nominal36=nominalsmall;
    uncerts36=uncertssmall;
    indages36=indagessmall;
    %siteindex36=siteindexsmalltemp;
    cov36=covsmall;
    
%Creating the age index and indages for the x val datasets
%initialize the matrix with the first sample
ageindexsmall(1)=1;
previousindex=ageindexsmalltemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewsmall(1,1:2)=indages36(1,1:2);
countindage=1;

for i=2:size(nominal36,1);
    currentindex=ageindexsmalltemp(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexsmall(i)=ageindexsmall(i-1);
    else
        countindage=countindage+1;
        ageindexsmall(i)=ageindexsmall(i-1)+1;
        indagenewsmall(countindage,1:2)=indages36(i,1:2);
        
    end
    previousindex=currentindex;
end

%sorting site index
nameindex36small(1)=siteindexsmalltemp(1);
previousnameindex=siteindexsmalltemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
countnameindex=1;
sitenames36small(1)=fullsitenames36(siteindexsmalltemp(1));
siteindex36small(1)=1;
for i=2:size(nominal36,1);
    currentnameindex=siteindexsmalltemp(i);
    if currentnameindex==previousnameindex;
        siteindex36small(i)=countnameindex;
    else
        countnameindex=countnameindex+1;
        nameindex36small(countnameindex)=siteindexsmalltemp(i);
        siteindex36small(i)=countnameindex;
        sitenames36small(countnameindex)=fullsitenames36(nameindex36(countnameindex));
    end
    previousnameindex=currentnameindex;
end

%rename variables
ageindex36=ageindexsmall;
indages36=indagenewsmall;
sitenames36=sitenames36small;
siteindex36=siteindex36small;
    save(smallname, 'nominal36', 'uncerts36', 'indages36','ageindex36','cov36','siteindex36','sitenames36')

% For the "big" matrix of values
    nominal36=nominalbig;
    uncerts36=uncertsbig;
    indages36=indagesbig;
    %siteindex36=siteindexbigtemp;
    cov36=covbig;
    
    %initialize the matrix with the first sample
ageindexbig(1)=1;
previousindex=ageindexbigtemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewbig(1,1:2)=indages36(1,1:2);
countindage=1;

for i=2:size(nominal36,1);
    currentindex=ageindexbigtemp(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexbig(i)=ageindexbig(i-1);
    else
        countindage=countindage+1;
        ageindexbig(i)=ageindexbig(i-1)+1;
        indagenewbig(countindage,1:2)=indages36(i,1:2);
        
    end
    previousindex=currentindex;
end

%sorting site index
nameindex36big(1)=siteindexbigtemp(1);
previousnameindex=siteindexbigtemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
countnameindex=1;
sitenames36big(1)=fullsitenames36(siteindexbigtemp(1));
siteindex36big(1)=1;
for i=2:size(nominal36,1);
    currentnameindex=siteindexbigtemp(i);
    if currentnameindex==previousnameindex;
        siteindex36big(i)=countnameindex;
    else
        countnameindex=countnameindex+1;
        nameindex36big(countnameindex)=siteindexbigtemp(i);
        siteindex36big(i)=countnameindex;
        sitenames36big(countnameindex)=fullsitenames36(nameindex36big(countnameindex));
    end
    previousnameindex=currentnameindex;
end

%rename variables
    
    ageindex36=ageindexbig;
    indages36=indagenewbig;
    sitenames36=sitenames36big;
    siteindex36=siteindex36big;
    save(bigname, 'nominal36','uncerts36', 'indages36','ageindex36','cov36','siteindex36','sitenames36')
    
    %clear variables so we can rewrite in the next site
    
    clear nominalsmall
    clear uncertssmall
    clear indagessmall
    clear nominalbig
    clear uncertsbig
    clear indagesbig
    clear ageindexbig
    clear ageindexsmall
    clear ageindexsmalltemp
    clear ageindexbigtemp
    clear indagenewsmall
    clear indagenewbig
    clear covsmall
    clear covbig
    clear siteindexsmalltemp
    clear siteindexbigtemp
    clear sitenames36big
    clear siteindex36big
    clear nameindex36big
    clear sitenames36small
    clear siteindex36small
    clear nameindex36small
end

end