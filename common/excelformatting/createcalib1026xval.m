function out=createcalib1026xval(sample)

%Uses standard input + 4 additional columns necessary for calibrations. 
% Creates the individual variables needed for calibration as well as the
% datasets necessary for cross-validation.  

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. Note that all the
%samples from a single site must be together in order for the calibration
%dataset to be produced correctly. 

% One row per sample with the following columns:

%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)       
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (mm/kyr)
%9. Sample 10-Be concentration (atoms of 10-Be/g of target)
%10. Sample 26-Al concentration (atoms of 26-Al/g of target)
%11. Inheritance for Be (atoms 10-Be/g of target)
%12. Inheritance for Al (atoms 26-Al/g of target)
%13. Lambdafe (g/cm^2)
%14. Depth to top of sample (g/cm^2)
%15. Year sampled (e.g. 2010)
%16-30. One-sigma uncertainties for each of the above inputs (1-15). 
%31. Independent age of the sample (years before AD 2010)
%32. Uncertainty on independent age of the sample (years)
%33. Age Index indicator (should increment for each new age, even within
%sites (ex. if boulder ages are allowed to vary within the site)
%34. Site indicator: should match the sites given for the calibration list
%here:
% 1 - Promontory Point & Tabernacle Hill
% 2 - New Zealand
% 3 - Peru
% 4 - Scotland
% 5 - New England
% 6 - Argentina (Ackert 2003) (ARG)
% 7 - Oregon (Cerling & Craig, 1994; Licciardi 1999) (OR)
% 8 - Idaho (Cerling & Craig, 1994) (ID)
% 9 - Canary Islands (Dunai & Wijbrans 2000) (CI)
%10 - Iceland (Licciardi 2006) (ICE)
%11 - Hawaii (Kurz 1990) (HAW)
%12 - Sicily (Blard 2006) (ETNA)

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
%go through each sample and see if it has Be and/or Al nd put it into the
%appropriate vectors (sample10 and sample26)
count10=1;
count26=1;

for i=1:numbersamps;

    if isnan(sample(i,9)) || sample(i,9)==0;
    else
        sample10(count10,:)=sample(i,:);
        count10=count10+1;
    end
    if isnan(sample(i,10)) || sample(i,10)==0; 
    else
        sample26(count26,:)=sample(i,:);
        count26=count26+1;
    end
end

if count10~=1
nominal10(:,1:15)=sample10(:,1:15);
uncerts10(:,1:15)=sample10(:,16:30);
indages10(:,1:2)=sample10(:,31:32);
ageindexsample10(:,1)=sample10(:,33);
siteindex10(:,1)=sample10(:,34);
%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal10(:,8)=nominal10(:,8).*nominal10(:,6)./10;
uncerts10(:,8)=uncerts10(:,8).*nominal10(:,6)./10;

%split up samples based on aging index/site index
% For Be
%initialize the matrix with the first sample
ageindex10(1)=1;
previousindex=ageindexsample10(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenew10(1,1:2)=indages10(1,1:2);
countindage=1;

for i=2:size(nominal10,1);
    currentindex=ageindexsample10(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindex10(i)=ageindex10(i-1);
    else
        countindage=countindage+1;
        ageindex10(i)=ageindex10(i-1)+1;
        indagenew10(countindage,1:2)=indages10(i,1:2);
        
    end
    previousindex=currentindex;
end
save calibset10test nominal10 uncerts10 indages10 ageindex10 indagenew10

%Below this point for Be - cross-validation datasets are produced

fullnominal10=nominal10;
fulluncerts10=uncerts10;
fullindages10=indages10;
fullageindex10=ageindex10';

%for Be
for i=1:4
    switch i
        case 1;
            smallname='PPTonly10';
            bigname='calib10minusPPT';
            countsiteindex=1;
            
        case 2;
            countsiteindex=2;
            smallname='NZonly10';
            bigname='calib10minusNZ';
            
        case 3;
            countsiteindex=3;
            smallname='HUonly10';
            bigname='calib10minusHU';
            
        case 4;
            countsiteindex=4;
            smallname='SCOTonly10';
            bigname='calib10minusSCOT';
    end
    countsmall=1;
    countbig=1;
    datafound=false;
    numbersamps=size(fullnominal10,1);
    for k=1:numbersamps;
        if siteindex10(k,1)==countsiteindex;
            %copy sample(k) to small matrix
            nominalsmall(countsmall,:)=fullnominal10(k,:);
            uncertssmall(countsmall,:)=fulluncerts10(k,:);
            indagessmall(countsmall,:)=fullindages10(k,:);
            ageindexsmalltemp(countsmall,:)=fullageindex10(k,:);
            countsmall=countsmall+1;
            datafound=true;
            %create the age index and indpt ages matrices
        else
            % copy sample(k) to big matrix
            nominalbig(countbig,:)=fullnominal10(k,:);
            uncertsbig(countbig,:)=fulluncerts10(k,:);
            indagesbig(countbig,:)=fullindages10(k,:);
            ageindexbigtemp(countbig,:)=fullageindex10(k,:);
            countbig=countbig+1;
        end
    end
    % save variables to the correct name (renaming them)
    if datafound==false
        continue
    end 
    nominal10=nominalsmall;
    uncerts10=uncertssmall;
    indages10=indagessmall;
    
%Creating the age index and indages for the x val datasets
% For Be
%initialize the matrix with the first sample
ageindexsmall(1)=1;
previousindex=ageindexsmalltemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewsmall(1,1:2)=indages10(1,1:2);
countindage=1;

for i=2:size(nominal10,1);
    currentindex=ageindexsmalltemp(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexsmall(i)=ageindexsmall(i-1);
    else
        countindage=countindage+1;
        ageindexsmall(i)=ageindexsmall(i-1)+1;
        indagenewsmall(countindage,1:2)=indages10(i,1:2);
        
    end
    previousindex=currentindex;
end
%rename variables
ageindex10=ageindexsmall;
indagenew10=indagenewsmall;
    save(smallname, 'nominal10', 'uncerts10', 'indages10','indagenew10','ageindex10')

% For the "big" matrix of values
    nominal10=nominalbig;
    uncerts10=uncertsbig;
    indages10=indagesbig;
    
    %initialize the matrix with the first sample
ageindexbig(1)=1;
previousindex=ageindexbigtemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewbig(1,1:2)=indages10(1,1:2);
countindage=1;

for i=2:size(nominal10,1);
    currentindex=ageindexbigtemp(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexbig(i)=ageindexbig(i-1);
    else
        countindage=countindage+1;
        ageindexbig(i)=ageindexbig(i-1)+1;
        indagenewbig(countindage,1:2)=indages10(i,1:2);
        
    end
    previousindex=currentindex;
end
%rename variables
    
    ageindex10=ageindexbig;
    indagenew10=indagenewbig;
    save(bigname, 'nominal10','uncerts10', 'indages10','indagenew10','ageindex10')
    
    %clear variables so we can rewrite in the next site
    
    clear nominalsmall
    clear uncertssmall
    clear indagessmall
    clear nominalbig
    clear uncertsbig
    clear indagesbig
    clear ageindexbig
    clear ageindexsmall
    clear ageindexsmalltemp
    clear ageindexbigtemp
    clear indagenewsmall
    clear indagenewbig
end

end

%Rest of code does same thing for Al
if count26~=1
nominal26(:,1:15)=sample26(:,1:15);
uncerts26(:,1:15)=sample26(:,16:30);
indages26(:,1:2)=sample26(:,31:32);
ageindexsample26(:,1)=sample26(:,33);
siteindex26(:,1)=sample26(:,34);
%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal26(:,8)=nominal26(:,8).*nominal26(:,6)./10;
uncerts26(:,8)=uncerts26(:,8).*nominal26(:,6)./10;

 % For Al
%initialize the matrix with the first sample
ageindex26(1)=1;
previousindex=ageindexsample26(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenew26(1,1:2)=indages26(1,1:2);
countindage=1;

for i=2:size(nominal26,1);
    currentindex=ageindexsample26(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindex26(i)=ageindex26(i-1);
    else
        countindage=countindage+1;
        ageindex26(i)=ageindex26(i-1)+1;
        indagenew26(countindage,1:2)=indages26(i,1:2);
    end
    previousindex=currentindex;
end   
 save calibset26test nominal26 uncerts26 indages26 ageindex26 indagenew26   
    %Below this point for Al - cross-validation datasets are produced
fullnominal26=nominal26;
fulluncerts26=uncerts26;
fullindages26=indages26;
fullageindex26=ageindex26';
    
%for Al
for p=1:4
    switch p
        case 1;
            smallname='PPTonly26';
            bigname='calib26minusPPT';
            countsiteindex=1;
            
        case 2;
            countsiteindex=2;
            smallname='NZonly26';
            bigname='calib26minusNZ';
            
        case 3;
            countsiteindex=3;
            smallname='HUonly26';
            bigname='calib26minusHU';
            
        case 4;
            countsiteindex=4;
            smallname='SCOTonly26';
            bigname='calib26minusSCOT';
    end
    countsmall=1;
    countbig=1;
    datafound=false;
    numbersamps=size(fullnominal26,1);
    for k=1:numbersamps;
        if siteindex26(k,1)==countsiteindex;
            %copy sample(k) to small matrix
            nominalsmall(countsmall,:)=fullnominal26(k,:);
            uncertssmall(countsmall,:)=fulluncerts26(k,:);
            indagessmall(countsmall,:)=fullindages26(k,:);
            ageindexsmalltemp(countsmall,:)=fullageindex26(k,:);
            countsmall=countsmall+1;
            datafound=true;
            %create the age index and indpt ages matrices
        else
            % copy sample(k) to big matrix
            nominalbig(countbig,:)=fullnominal26(k,:);
            uncertsbig(countbig,:)=fulluncerts26(k,:);
            indagesbig(countbig,:)=fullindages26(k,:);
            ageindexbigtemp(countbig,:)=fullageindex26(k,:);
            countbig=countbig+1;
        end
    end
    % save variables to the correct name (renaming them)
    if datafound==false
        continue
    end
    nominal26=nominalsmall;
    uncerts26=uncertssmall;
    indages26=indagessmall;
    
%Creating the age index and indages for the x val datasets
% For Al
%initialize the matrix with the first sample
ageindexsmall(1)=1;
previousindex=ageindexsmalltemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewsmall(1,1:2)=indages26(1,1:2);
countindage=1;

for q=2:size(nominal26,1);
    currentindex=ageindexsmalltemp(q);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexsmall(q)=ageindexsmall(q-1);
    else
        countindage=countindage+1;
        ageindexsmall(q)=ageindexsmall(q-1)+1;
        indagenewsmall(countindage,1:2)=indages26(q,1:2);
        
    end
    previousindex=currentindex;
end
%rename variables
ageindex26=ageindexsmall;
indagenew26=indagenewsmall;
    save(smallname, 'nominal26', 'uncerts26', 'indages26','indagenew26','ageindex26')

% For the "big" matrix of values
    nominal26=nominalbig;
    uncerts26=uncertsbig;
    indages26=indagesbig;
    
    %initialize the matrix with the first sample
ageindexbig(1)=1;
previousindex=ageindexbigtemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewbig(1,1:2)=indages26(1,1:2);
countindage=1;

for i=2:size(nominal26,1);
    currentindex=ageindexbigtemp(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexbig(i)=ageindexbig(i-1);
    else
        countindage=countindage+1;
        ageindexbig(i)=ageindexbig(i-1)+1;
        indagenewbig(countindage,1:2)=indages26(i,1:2);
        
    end
    previousindex=currentindex;
end
%rename variables
    
    ageindex26=ageindexbig;
    indagenew26=indagenewbig;
    save(bigname, 'nominal26','uncerts26', 'indages26','indagenew26','ageindex26')
    
    %clear variables so we can rewrite in the next site
    
    clear nominalsmall
    clear uncertssmall
    clear indagessmall
    clear nominalbig
    clear uncertsbig
    clear indagesbig
    clear ageindexbig
    clear ageindexsmall
    clear ageindexsmalltemp
    clear ageindexbigtemp
    clear indagenewsmall
    clear indagenewbig
end
    
end       
    
    
            
            
            
            
            
            
            
            
            
            
            
            
            