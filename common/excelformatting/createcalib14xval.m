function out=createcalib14xval(sample)

%Uses standard input + 4 additional columns necessary for calibrations. 
% Creates the individual variables needed for calibration as well as the
% datasets necessary for cross-validation. 

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. 
% One row per sample with the following columns:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (mm/kyr)
%9. Sample 14-C concentration (atoms of 14-C/g of target)
%10. Inheritance for 14-C (atoms 14-C/g of target)
%11. Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%14-26. Uncertainty for each of the above variables
%27. Independent age of the sample (years before AD 2010)
%28. Uncertainty on independent age of the sample (years)
%29. Age Index indicator (should increment for each new age, even within
%sites (ex. if boulder ages are allowed to vary within the site)
%34. Site indicator: should match the sites given for the calibration list
%here:
% 1 - Promontory Point & Tabernacle Hill
% 2 - New Zealand
% 3 - Peru
% 4 - Scotland
% 5 - New England
% 6 - Argentina (Ackert 2003) (ARG)
% 7 - Oregon (Cerling & Craig, 1994; Licciardi 1999) (OR)
% 8 - Idaho (Cerling & Craig, 1994) (ID)
% 9 - Canary Islands (Dunai & Wijbrans 2000) (CI)
%10 - Iceland (Licciardi 2006) (ICE)
%11 - Hawaii (Kurz 1990) (HAW)
%12 - Sicily (Blard 2006) (ETNA)
%13 - Antarctica
%14 - Sierra Nevada/White Mountains
%15 - Chile (Transect)

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
nominal14=zeros(numbersamps,13);
indages14=zeros(numbersamps,2);
uncerts14=zeros(numbersamps,13);
ageindexsample14=zeros(numbersamps,1);
siteindex14=zeros(numbersamps,1);

nominal14(:,1:13)=sample(:,1:13);
uncerts14(:,1:13)=sample(:,14:26);
indages14(:,1:2)=sample(:,27:28);
ageindexsample14(:,1)=sample(:,29);
siteindex14(:,1)=sample(:,30);

%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal14(:,8)=nominal14(:,8).*nominal14(:,6)./10;
uncerts14(:,8)=uncerts14(:,8).*nominal14(:,6)./10;

%split up samples based on aging index/site index
%initialize the matrix with the first sample
ageindex14(1)=1;
previousindex=ageindexsample14(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenew14(1,1:2)=indages14(1,1:2);
countindage=1;

for i=2:size(nominal14,1);
    currentindex=ageindexsample14(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindex14(i)=ageindex14(i-1);
    else
        countindage=countindage+1;
        ageindex14(i)=ageindex14(i-1)+1;
        indagenew14(countindage,1:2)=indages14(i,1:2);
        
    end
    previousindex=currentindex;
end
save calibset14test nominal14 uncerts14 indages14 ageindex14 indagenew14

%Below this point - cross-validation datasets are produced

fullnominal14=nominal14;
fulluncerts14=uncerts14;
fullindages14=indages14;
fullageindex14=ageindex14';

for i=1:4
    switch i
        case 1;
            smallname='PPTonly14';
            bigname='calib14minusPPT';
            countsiteindex=1;
            
        case 2;
            countsiteindex=2;
            smallname='NZonly14';
            bigname='calib14minusNZ';
            
        case 3;
            countsiteindex=3;
            smallname='HUonly14';
            bigname='calib14minusHU';
            
        case 4;
            countsiteindex=4;
            smallname='SCOTonly14';
            bigname='calib14minusSCOT';
    end
    countsmall=1;
    countbig=1;
    datafound=false;
    numbersamps=size(fullnominal14,1);
    for k=1:numbersamps;
        if siteindex14(k,1)==countsiteindex;
            %copy sample(k) to small matrix
            nominalsmall(countsmall,:)=fullnominal14(k,:);
            uncertssmall(countsmall,:)=fulluncerts14(k,:);
            indagessmall(countsmall,:)=fullindages14(k,:);
            ageindexsmalltemp(countsmall,:)=fullageindex14(k,:);
            countsmall=countsmall+1;
            datafound=true;
            %create the age index and indpt ages matrices
        else
            % copy sample(k) to big matrix
            nominalbig(countbig,:)=fullnominal14(k,:);
            uncertsbig(countbig,:)=fulluncerts14(k,:);
            indagesbig(countbig,:)=fullindages14(k,:);
            ageindexbigtemp(countbig,:)=fullageindex14(k,:);
            countbig=countbig+1;
        end
    end
    % save variables to the correct name (renaming them)
    if datafound==false
        continue
    end 
    nominal14=nominalsmall;
    uncerts14=uncertssmall;
    indages14=indagessmall;
    
%Creating the age index and indages for the x val datasets
%initialize the matrix with the first sample
ageindexsmall(1)=1;
previousindex=ageindexsmalltemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewsmall(1,1:2)=indages14(1,1:2);
countindage=1;

for i=2:size(nominal14,1);
    currentindex=ageindexsmalltemp(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexsmall(i)=ageindexsmall(i-1);
    else
        countindage=countindage+1;
        ageindexsmall(i)=ageindexsmall(i-1)+1;
        indagenewsmall(countindage,1:2)=indages14(i,1:2);
        
    end
    previousindex=currentindex;
end
%rename variables
ageindex14=ageindexsmall;
indagenew14=indagenewsmall;
    save(smallname, 'nominal14', 'uncerts14', 'indages14','indagenew14','ageindex14')

% For the "big" matrix of values
    nominal14=nominalbig;
    uncerts14=uncertsbig;
    indages14=indagesbig;
    
    %initialize the matrix with the first sample
ageindexbig(1)=1;
previousindex=ageindexbigtemp(1);
%indagenew is the variable for the new aging method. Indages is the
%variable that has the independent age for each sample.
indagenewbig(1,1:2)=indages14(1,1:2);
countindage=1;

for i=2:size(nominal14,1);
    currentindex=ageindexbigtemp(i);
    if currentindex==previousindex;
        %same age as previous sample, so only use one age
        ageindexbig(i)=ageindexbig(i-1);
    else
        countindage=countindage+1;
        ageindexbig(i)=ageindexbig(i-1)+1;
        indagenewbig(countindage,1:2)=indages14(i,1:2);
        
    end
    previousindex=currentindex;
end
%rename variables
    
    ageindex14=ageindexbig;
    indagenew14=indagenewbig;
    save(bigname, 'nominal14','uncerts14', 'indages14','indagenew14','ageindex14')
    
    %clear variables so we can rewrite in the next site
    
    clear nominalsmall
    clear uncertssmall
    clear indagessmall
    clear nominalbig
    clear uncertsbig
    clear indagesbig
    clear ageindexbig
    clear ageindexsmall
    clear ageindexsmalltemp
    clear ageindexbigtemp
    clear indagenewsmall
    clear indagenewbig
end

end
