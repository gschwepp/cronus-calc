
function out=usingERA40atm(sample);

%sample has one row per sample and each sample has 3 columns: latitude,
%longitude, and elevation

numbersamples=size(sample,1);
%initialize the results vectors
atm=zeros(numbersamples,1);

for i=1:numbersamples;
    atm(i)=ERA40atm(sample(i,1),sample(i,2),sample(i,3));
end
out.atm=atm;

