%
%  [Rm,Ccl]=spiketorm(spikeconc,spikemass,A0,samplemass,RS,SS)
%
function [Rm,Ccl]=spiketorm(spikeconc,spikemass,A0,samplemass,RS, ...
			    SS)
%
% Useful constants.
%
Wn=35.4527;            % Atomic weight of Cl.
W35=34.96885;          % Atomic weight of 35-Cl.
W37=36.96801;          % Atomic weight of 37-Cl.
AN=0.7572816;          % Natural isotope ratio for 35-Cl/Cl.
%
% First, the concentration of Cl.
%
Ccl=1000*spikeconc*spikemass*Wn*(A0-SS*(1-A0))/...
    (samplemass*((A0*W35+(1-A0)*W37)*(SS*(1-AN)-AN)));
%
% Next Rm.
%
Rm=RS*(1+((1-AN)*SS-AN)/(A0-SS*(1-A0)));
