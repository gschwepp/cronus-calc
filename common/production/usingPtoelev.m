function elev=usingPtoelev(sample)
numbersamples=size(sample,1);

for i=1:numbersamples
    i
    elev(i)=Ptoelev(sample(i,1),sample(i,2),sample(i,3))
end
