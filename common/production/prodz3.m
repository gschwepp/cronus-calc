%
% [ProdtotalHe,ProdsHe,ProdmuHe]=prodz3(z,pp,sf,cp)
%
%
% The prodz function calculates spallation production of 3He
% at a particular given depth. 

%  pp,sf,cp               physics, site, sample, and
%                         computed parameters.
%  z                      Depths in g/cm^2.
% 
% Get current scaling factors. 
% Then call this function to compute total production for a vector of
% depths.  
% Note: This function has been updated to work with nuclide-dependent
% scaling factors (only applicable to the Sato/Lifton scaling scheme).  
%
function [ProdtotalHe,ProdsHe]=prodz3(z,pp,sf,cp)
%
% Make sure that we don't have any depths that are too big.
%
if (max(z) > cp.maxdepth)
  error('Prodz called for depth greater than maxdepth.');
end
%
% Make sure that sf.SelX is a number.
%
if (isnan(sf.currentsf.Sel3))
    error('sf.currentsf.Sel3 is a NaN');
end
%
% We'll get some slightly negative depths due to roundoff errors.
% Fix them.
%
for i=1:length(z)
  if (z(i)<-1.0e-4)
    error('z(i) is negative!');
  end
  if (z(i)<0.0)
    z(i)=0.0;
  end
end

%Snow shielding
% Snow shielding correction according to Zweck et al. (2013). 
zsnow=400; %depth of snow cover [cm]
rhosnow=0.5; %density of snow [g/cm3]
zcover=zsnow*rhosnow; %snow mass length [g/cm2]
covertime=0.0; %fraction of time when cover is present. This is applied to 
%each time step. Example: if a time step is 100 years, this would assume 
%that the cover was present for half of that time. 

%Zweck et al. (2013) constants for different composition rocks - leave only one option (a and
%b)uncommented. Uncertainties (sigma) - not used at this time. Assumes
%samples at surface.

% Siliceous dolomite
%a=[1.51 -0.428 0.37 740];
%b=[3.374 -0.0251 -2.228 -0.611 1.0166];
%sigmaa=[0.13 0.014 0.12 220];
%sigmab=[0.060 0.0013 0.069 0.040 0.0006];

% Basalt
%a=[1.87 -0.388 0.46 1000];
%b=[3.786 -0.0233 -2.604 -0.745 1.0194];
%sigmaa=[0.20 0.015 0.16 430];
%sigmab=[0.057 0.0011 0.073 0.046 0.0006];

% Granite
%a=[1.81 -0.391 0.44 930];
%b=[3.701 -0.0238 -2.525 -0.697 1.0185];
%sigmaa=[0.18 0.014 0.15 350];
%sigmab=[0.056 0.0011 0.069 0.041 0.0005];

snows=covertime*exp(-zcover/cp.Lambdafe)+(1-covertime); %Produces only 1 value - does 
% not depend on depth or composition

%snoweth=covertime*((a(1)*zcover+1)^(a(2))-(((sp.ls*sp.rb)*zcover^(a(3)))/a(4)))+(1-covertime);
% snoweth is depth-dependent so it produces a vector with one scaling
% factor for each depth

%if zsnow>0
%    snowth=covertime*((b(1)*exp(b(2)*zcover)+b(3)*exp(b(4)*zcover))*b(5).^(-(sp.ls*sp.rb)))+(1-covertime);
%else
%    snowth=1;
%end
    % snowth is depth-dependent so it produces a vector with one scaling
% factor for each depth

%find the number of depths given in the input vector z
numberdepths=length(z);
%
% Production from spallation is all that is currently included for He.
%
ProdsHe=sf.currentsf.Sel3*sf.ST*snows*pp.PsHe*exp(-z/cp.Lambdafe);
%
% Now, compute the total production.
%
ProdtotalHe=ProdsHe;
%
% Look for NaN's in the results.
%
if ((sum(isnan(ProdtotalHe)) > 0) )
  ProdtotalHe
  error('Prodz3 produced NaN!');
end


