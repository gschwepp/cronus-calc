
function [ total, conc10, conc26 ] = usingpredN1026( scaling_model, inputfilename, outputfilename )
  
load( inputfilename, '-mat' );
%
% Get basic info about the sample ages.
%
nsamples=size(nominal110,1);
pp=physpars();
maxage=2000;

for i=1:nsamples;
  sp=samppars1026(nominal10(i,:));
  sf=scalefacs1026(sp,scaling_model);

  % Figure out the maximum possible depth at which we'll ever need a
  % production rate.  This is depthtotop + maxage * erosion (g/cm^2/kyr)+
  % thickness * density + a safety factor.
  %
  maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
  
  cp=comppars1026(pp,sp,sf,maxdepth);
  age=ages(i);
  [conc10(i),conc26(i)]=predN1026(pp,sp,sf,cp,age,scaling_model);
end

total=zeros(nsamples,2);
total(:,1)=conc10;
total(:,2)=conc26;

if nargin > 2
  % Save the results.
  %
  save( outputfilename );
end
