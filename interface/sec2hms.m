function hms = sec2hms(toc_time)
    hours = floor(toc_time/3600);
    toc_time = toc_time - hours * 3600;
    mins = floor(toc_time / 60);
    secs = toc_time - mins * 60;
	if (hours > 0)
		hms = sprintf('%02d:%02d:%02.0f', hours, mins, secs);
	else
		hms = sprintf('%02d:%02.0f', mins, secs);
	end
end
