function [age_string,uncert_string] = age2str(age_value, uncert_value, uncert_sigfigs)
	% Outputs the given age and uncert properly formatted based on the given number of uncert sigfigs
	
	%num2str with sigfigs may results in exponential format so convert it back to a number
	%	then back to a string with no sigfigs (since they have already been accounted for)
	uncert_string = num2str(str2num(num2str(uncert_value,uncert_sigfigs)));
	
	%Split the uncert string into the integer and decimal portion
	[UncertInts,UncertDecimals] = strtok(uncert_string,'.');
	
	% Get the number of decimals (or 0) - must subtract one since strtok leaves in the '.'
	UncertNumDecimals = max(length(UncertDecimals) - 1, 0);
	
	%Get the number of non-sigfig zeros (only the case if uncert has no decimal part)
	UncertNumZeros = 0;
	if (UncertNumDecimals == 0)
		for index=length(UncertInts):-1:1,
			if (UncertInts(index) == '0')
				UncertNumZeros = UncertNumZeros + 1;
			else
				break;
			end
		end
	end
	
	%Determine the sigfigs to use for age
	%	1) if UncertNumDecimals is more than 0, include that many decimals in age
	%	2) otherwise, age sigfigs is the length of age - the number of non-sigfig zeroes in the uncertainty integer
	%		so if uncertainty is 1234 and age is 1234567, then with uncert_sigfigs of 2 uncertainty is 1200 and age is 1234500
	% This can be summed up as length(AgeInts) - UncertNumZeros + UncertNumDecimals
	% Case 1) UncertNumZeros = 0, UncertNumDecimals > 0 => length(AgeInts) - 0 + UncertNumDecimals
	% Case 2) UncertNumZeros >= 0, UncertNumDecimals = 0 => length(AgeInts) - UncertNumZeros + 0
	AgeInts = strtok(num2str(age_value),'.');
	AgeSigfigs = length(AgeInts) - UncertNumZeros + UncertNumDecimals;
	age_string = num2str(str2num(num2str(age_value,AgeSigfigs)));
end

