function lprint(logFile,msg,varargin)
   if (isempty(logFile)) return; end;
   log_id = fopen(logFile,'a');
   if (numel(varargin) > 0)
      Message = sprintf(msg,varargin{:});
   else
      Message = msg;
   end
   fprintf(log_id,['[' datestr(now) '] ' Message]);
   fprintf(['[' datestr(now) '] ' Message]);
   fclose(log_id);   
end
